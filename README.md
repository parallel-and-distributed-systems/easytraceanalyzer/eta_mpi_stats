# eta_mpi_stats

*A tool that provides statistics on the use of MPI functions for a given trace*

Usage: `src/eta_mpi_stats [options] [arguments...]`

Available options:

| Option               | Description                                                      |
| -------------------- | ---------------------------------------------------------------- |
| `-h`, `--help`       | Print the help screen                                            |
| `-p`, `--process`    | Output stats by process                                          |
| `-c`, `--cumulative` | Output total cumulative stats                                    |
| `-m`, `--message`    | Dump messages in a file and compute the communication matrix     |
| `-t`, `--tag`        | Select a single tag to dump messages                             |
| `-e`, `--exclusive`  | Compute the exclusive time of execution instead of the inclusive |

By default, stats are shown thread by thread. Use `-p` or `-c` to regroup them.

## Example

```
$ src/eta_mpi_stats -m ../traces/eztrace_bt.A.4.trace
Opening ../traces/eztrace_bt.A.4.trace
Process #1: thread #4160637728
	Total duration: 23556.1
	Messages sent: 2418
	Messages size (in bytes): min: 43560 ; max: 261360 ; avg: 117102 ; total: 283153800

	                 Function       Count       Total   % runtime         Min         Max        Avg
	-------------------------.-----------.-----------.-----------.-----------.-----------.-----------
	             STV_MPI_Recv        2418     1770.66      7.5168           0     3.12289    0.732285
	             STV_MPI_Send        2418     1254.42     5.32525           0     5.38119    0.518785
	          STV_MPI_Waitall         202           0           0           0           0           0
	            STV_MPI_BCast           6    0.245288  0.00104129    0.000351    0.185044   0.0408813
	        STV_MPI_Allreduce           2     1.59749  0.00678162    0.344778     1.25271    0.798743
	          STV_MPI_Barrier           2    0.520258  0.00220859    0.038363    0.481895    0.260129
	           STV_MPI_Reduce           1    0.018023 7.65109e-05    0.018023    0.018023    0.018023
	-------------------------.-----------.-----------.-----------.-----------.-----------.-----------
	                    TOTAL        5049     3027.47     12.8522

... other threads ...

Messages dumped in data_messages.dat
Use gnuplot plot.plt to display MPI communication matrix
0: Container(2) -> "P#0_T#4160637728"
1: Container(4) -> "P#1_T#4160637728"
2: Container(6) -> "P#2_T#4160637728"
3: Container(8) -> "P#3_T#4160637728"
```

Content of the file `data_messages.dat`:

```
src dst len tag start time  end time    throughput
Container(2)    Container(4)    81920   3000    48.5993     53.9958     15180.2
Container(2)    Container(4)    81920   2000    48.5738     53.996      15108.2
Container(8)    Container(4)    81920   5000    49.389      53.9961     17781.3
Container(8)    Container(4)    81920   4000    49.3596     53.9962     17668
...
```

## Communication matrix

Use `$ gnuplot plot.plt` to generate the files `mpi_matrix_count.png` and `mpi_matrix_size.png`. 
The first one contains the data necessary to plot a matrix that shows the number of messages sent from one MPI rank to another, and the second file the amount of data (in bytes) sent from a MPI rank to another.

Example of picture generated:

<div align="center">

![](doc/mpi_matrix.png)
</div>
