N = floor(system("wc -l data_matrix_count.dat"))
set term png
set xrange [-.5:N-.5]
set yrange [-.5:N-.5]
set xlabel "to"
set ylabel "from"
# set xtics 0,1,N
# set ytics 0,1,N
set key off
set output "mpi_matrix_count.png"
set title "MPI Communication Matrix - count of messages"
plot "./data_matrix_count.dat" matrix with image
set output "mpi_matrix_size.png"
set title "MPI Communication Matrix - total size of messages (bytes)"
plot "./data_matrix_size.dat" matrix with image
