#include "eta_mpi_stats.h"
#include "output.h"

std::unordered_map<std::string, ct_stats> stats;

// storage of all messages
std::vector<message> msg_vector;
// to compute the exclusive runtime
std::unordered_map<std::string, std::vector<std::string>> event_vector;

std::optional<eta::Value> getFieldValue(eta::Trace const &t,
                                        eta::Event const &e,
                                        std::string const &field_id) {
  for (auto const &field : e.fields) {
    if (t.fields.get_field_name(field.id) == field_id) {
      return field.value;
    }
  }

  return std::nullopt;
}

void registerEvent(eta::Trace const &t, eta::Event const &e,
                   int const &selected_tag, bool const &exclusive) {
  auto const ct_name = t.container(e.container_id).name;

  switch (e.type) {
  case eta::EventType::EnterFunction: {
    auto has_exit = false;
    auto const exit_event_value = getFieldValue(t, e, "ExitFunctionEvent");
    auto const f_name_value = getFieldValue(t, e, "FunctionName");
    eta::Event exit_event;

    if (exit_event_value.has_value()) {
      has_exit = true;
      exit_event = t.event(std::get<eta::EventId>(exit_event_value.value()));
    }

    if (!f_name_value.has_value()) {
      std::cerr << "Field FunctionName not found\n";
      std::exit(1);
    }

    auto f_name = eta::to_string(f_name_value.value());
    f_name.erase(remove(f_name.begin(), f_name.end(), '\"'), f_name.end());

    if (std::find(mpi_functions.begin(), mpi_functions.end(), f_name) !=
        mpi_functions.end()) {
      auto &stats_ct = stats[ct_name];
      auto &function_stats = stats_ct.fstats_map[f_name];

      ++function_stats.count;

      if (has_exit) {
        auto const event_duration = exit_event.timestamp - e.timestamp;

        function_stats.total += event_duration;

        if (!exclusive) {
          if (function_stats.count == 1) {
            function_stats.min = event_duration;
            function_stats.max = event_duration;
          }

          if (event_duration < function_stats.min) {
            function_stats.min = event_duration;
          } else if (event_duration > function_stats.max) {
            function_stats.max = event_duration;
          }
        } else {
          event_vector[ct_name].push_back(f_name);
        }
      }
    }
    break;
  }
  case eta::EventType::ExitFunction: {
    if (exclusive) {
      auto const enter_event_value = getFieldValue(t, e, "EnterFunctionEvent");
      auto const f_name_value = getFieldValue(t, e, "FunctionName");

      if (!f_name_value.has_value()) {
        std::cerr << "Field FunctionName not found\n";
        std::exit(1);
      }

      auto const enter_event =
          t.event(std::get<eta::EventId>(enter_event_value.value()));

      auto f_name = eta::to_string(f_name_value.value());
      f_name.erase(remove(f_name.begin(), f_name.end(), '\"'), f_name.end());

      if (std::find(mpi_functions.begin(), mpi_functions.end(), f_name) !=
          mpi_functions.end()) {
        event_vector[ct_name].pop_back();

        if (!event_vector[ct_name].empty()) {
          auto const event_duration = e.timestamp - enter_event.timestamp;
          auto const name = event_vector[ct_name].back();

          stats[ct_name].fstats_map[name].total -= event_duration;
        }
      }
    }
    break;
  }
  }
}

void registerTrace(eta::Trace const &t, int const &selected_tag,
                   bool const &exclusive) {
  for (auto const &event : t.events) {
    if (event.container_id.isValid() &&
        t.container(event.container_id).is_terminal) {
      registerEvent(t, event, selected_tag, exclusive);
    }
  }
}

void registerCommunications(eta::Trace const &t, int const &selected_tag,
                            bool const &dump_messages) {
  for (auto const &c : t.communications) {
    if (c.src.isValid() && (c.tag == selected_tag || selected_tag < 0)) {
      auto const ct_name = t.container(c.src).name;

      auto &msg_stats = stats[ct_name].msg_stats;
      if (msg_stats.min == 0 && msg_stats.max == 0) {
        msg_stats.min = c.len;
        msg_stats.max = c.len;
      }

      if (c.len < msg_stats.min)
        msg_stats.min = c.len;
      else if (c.len > msg_stats.max)
        msg_stats.max = c.len;

      msg_stats.total += c.len;
      msg_stats.count++;

      if (dump_messages) {
        if (c.type == eta::CommunicationType::P2P) {
          message msg;
          msg.len = c.len;
          msg.tag = c.tag;
          msg.type = c.type;
          msg.src = c.src;
          msg.dst = c.dests[0];
          msg.startDate = t.event(c.from[0]).timestamp;
          msg.endDate = t.event(c.to[0]).timestamp;

          msg_vector.emplace_back(std::move(msg));
        } else {
          for (auto const &dest : c.dests) {
            message msg;
            msg.len = c.len;
            msg.tag = c.tag;
            msg.type = c.type;
            msg.src = c.src;
            msg.dst = dest;

            auto evt_from = std::find_if(
                c.from.begin(), c.from.end(), [&](auto const &tmp) {
                  return t.event(tmp).container_id == dest;
                });

            auto evt_to =
                std::find_if(c.to.begin(), c.to.end(), [&](auto const &tmp) {
                  return t.event(tmp).container_id == dest;
                });

            msg.startDate = t.event(*evt_from).timestamp;
            msg.endDate = t.event(*evt_to).timestamp;

            msg_vector.emplace_back(std::move(msg));
          }
        }
      }
    }
  }
}

/**
 * Process stats for the different containers of the trace:
 *   - duration of the container,
 *   - process id of the container, if necessary,
 *   - thread id of the container, if necessary.
 */
void process_ct_stats(eta::Trace const &t) {
  for (auto const &cont : t.containers) {
    if (std::count_if(t.containers.begin(), t.containers.end(),
                      [cont](auto const &c) { return c.parent == cont.id; }) ==
        0) {
      if (!cont.events.empty()) {
        stats[cont.name].duration += t.event(cont.events.back()).timestamp -
                                     t.event(cont.events.front()).timestamp;
      } else {
        stats[cont.name].duration += eta::Date(0);
      }
    }

    int proc = -1;
    long thread = -1;

    auto cont_name = cont.name;
    cont_name.erase(remove(cont_name.begin(), cont_name.end(), '\"'),
                    cont_name.end());
    auto ret = sscanf(cont_name.c_str(), "P#%d_T#%lu", &proc, &thread);

    switch (ret) {
    case 2:
      stats[cont.name].thread_id = thread;
      [[fallthrough]];
    case 1:
      stats[cont.name].process_id = proc;
      break;
    default:
      break;
    }
  }
}

int main(int argc, char **argv) {
  po::parser programOptions;
  auto selected_tag = -1;
  constexpr auto process_option_name = "process";
  constexpr auto cumulative_option_name = "cumulative";
  constexpr auto message_option_name = "message";
  constexpr auto tag_option_name = "tag";
  constexpr auto exclusive_option_name = "exclusive";
  constexpr auto verbose_option_name = "verbose";

  std::vector<std::string> files;
  programOptions[""].bind(files);

  programOptions["help"].abbreviation('h').single().description(
      "Print this help screen");
  programOptions[process_option_name].abbreviation('p').single().description(
      "Output stats by process");
  programOptions[cumulative_option_name].abbreviation('c').single().description(
      "Output total cumulative stats");
  programOptions[message_option_name].abbreviation('m').single().description(
      "Dump messages");
  programOptions[tag_option_name].abbreviation('t').type(po::u32).description(
      "Select a tag in dumped messages");
  programOptions[exclusive_option_name].abbreviation('e').description(
      "Compute the exclusive time of execution instead of the inclusive");
  programOptions[verbose_option_name].abbreviation('v').description(
      "Enable verbose mode");

  if (!programOptions(argc, argv)) {
    eta::log_critical("Error occured during cli arguments parsing. Aborting");
    return EXIT_FAILURE;
  }

  if (programOptions["help"].size() > 0) {
    std::cout << programOptions << '\n';
    return EXIT_SUCCESS;
  }

  if (programOptions["verbose"].size() > 0) {
    eta::log_level = eta::log_level_t::debug;
  }

  if (programOptions[tag_option_name].size() > 0) {
    selected_tag = programOptions["tag"].get().u32;
  }

  auto const output_process = programOptions[process_option_name].size() > 0;
  auto const output_cumulative =
      programOptions[cumulative_option_name].size() > 0;
  auto const dump_messages = programOptions[message_option_name].size() > 0;
  auto const exclusive = programOptions[exclusive_option_name].size() > 0;

  for (auto const &file : files) {
    std::cout << "Opening " << file << std::endl;
    auto const trace = eta::loadTrace(eta::Path(file));

    if (std::holds_alternative<eta::ParsingError>(trace)) {
      std::cerr << "Error parsing " << file << std::endl;
      return EXIT_FAILURE;
    }

    auto const &t = std::get<eta::Trace>(trace);

    registerTrace(t, selected_tag, exclusive);
    registerCommunications(t, selected_tag, dump_messages);
    process_ct_stats(t);
    outputResultGlobal(t, stats, msg_vector, dump_messages, output_process,
                       output_cumulative);
  }
}
