#pragma once
 
#define INSTALL_PREFIX "/usr/local"

#include <eta/core/ProgramOptions.hxx>
#include <eta/trace/loader.hpp>
#include <string>
#include <vector>

struct timestat {
  int count = 0;
  eta::Date total = 0;
  eta::Date min = 0;
  eta::Date max = 0;
};

struct msgstat {
  int count = 0;
  eta::Int min = 0;
  eta::Int max = 0;
  eta::Int total = 0;
};

struct message {
  eta::ContainerId src;
  eta::ContainerId dst;
  eta::Int len;
  eta::Int tag;
  eta::Date startDate = 0;
  eta::Date endDate = 0;
  eta::CommunicationType type;
};

struct ct_stats {
  std::unordered_map<std::string, timestat> fstats_map;
  msgstat msg_stats;
  eta::Date duration = 0;
  int process_id;
  long thread_id;
};

const std::vector<std::string> mpi_functions{
    /* custom */
    "mpi_wait_",
    "mpi_waitall_",
    "mpi_isend_",
    "mpi_irecv_",
    "mpi_send_",
    "mpi_recv_",
    "mpi_allreduce_",
    "mpi_barrier_",
    "MPI_Comm_dup",
    "mpi_bcast_",
    "MPI_Comm_split",
    "mpi_reduce_",
    /* should be the proper function name */
    "MPI_Send",
    "MPI_Ssend",
    "MPI_Isend",
    "MPI_Issend",
    "MPI_Recv",
    "MPI_Irecv",
    "MPI_Barrier",
    "MPI_BCast",
    "MPI_Gather",
    "MPI_Scatter",
    "MPI_Reduce",
    "MPI_Alltoall",
    "MPI_Allgather",
    "MPI_Reduce_scatter",
    "MPI_Allreduce",
    "MPI_Ibarrier",
    "MPI_Ibcast",
    "MPI_Igather",
    "MPI_Iscatter",
    "MPI_Ireduce",
    "MPI_Ialltoall",
    "MPI_Iallgather",
    "MPI_Ireduce_scatter",
    "MPI_Iallreduce",
    "MPI_Wait",
    "MPI_Waitall",
    "MPI_Waitany",
    "MPI_Test",
    "MPI_Testall",
    "MPI_Testany",
    /* Paje */
    "STV_MPI_Send",
    "STV_MPI_Ssend",
    "STV_MPI_Isend",
    "STV_MPI_Issend",
    "STV_MPI_Recv",
    "STV_MPI_Irecv",
    "STV_MPI_Barrier",
    "STV_MPI_BCast",
    "STV_MPI_Gather",
    "STV_MPI_Scatter",
    "STV_MPI_Reduce",
    "STV_MPI_Alltoall",
    "STV_MPI_Allgather",
    "STV_MPI_Reduce_scatter",
    "STV_MPI_Allreduce",
    "STV_MPI_Ibarrier",
    "STV_MPI_Ibcast",
    "STV_MPI_Igather",
    "STV_MPI_Iscatter",
    "STV_MPI_Ireduce",
    "STV_MPI_Ialltoall",
    "STV_MPI_Iallgather",
    "STV_MPI_Ireduce_scatter",
    "STV_MPI_Iallreduce",
    "STV_MPI_Wait",
    "STV_MPI_Waitall",
    "STV_MPI_Waitany",
    "STV_MPI_Test",
    "STV_MPI_Testall",
    "STV_MPI_Testany",
};
