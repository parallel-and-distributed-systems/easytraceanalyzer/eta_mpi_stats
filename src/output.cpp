#include "output.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>

void outputFunctions(
    std::unordered_map<std::string, timestat> const &fstats_map,
    eta::Date total_duration) {
  std::vector<std::pair<std::string, timestat>> tmp;
  eta::Date mpi_duration = 0;
  int mpi_count = 0;

  for (auto const &f : fstats_map) {
    tmp.push_back(f);
  }

  std::sort(tmp.begin(), tmp.end(), [&](auto const &a, auto const &b) {
    if (a.second.count == b.second.count)
      return a.second.total > b.second.total;

    return a.second.count > b.second.count;
  });

  std::cout << "\n\t" << std::setw(25) << "Function" << std::setw(12) << "Count"
            << std::setw(12) << "Total" << std::setw(12) << "% runtime"
            << std::setw(12) << "Min" << std::setw(12) << "Max" << std::setw(12)
            << "Avg\n"
            << "\t-------------------------.-----------.-----------.-----------"
               ".-----------.-----------.-----------\n";

  for (auto const &p : tmp) {
    mpi_duration += p.second.total;
    mpi_count += p.second.count;

    std::cout << "\t" << std::setw(25) << p.first << std::setw(12)
              << p.second.count << std::setw(12) << nlc::rm_unit(p.second.total)
              << std::setw(12)
              << nlc::rm_unit(p.second.total) / nlc::rm_unit(total_duration) *
                     100
              << std::setw(12) << nlc::rm_unit(p.second.min) << std::setw(12)
              << nlc::rm_unit(p.second.max) << std::setw(12)
              << nlc::rm_unit(p.second.total) / p.second.count;
    std::cout << "\n";
  }
  std::cout << "\t-------------------------.-----------.-----------.-----------"
               ".-----------.-----------.-----------\n";
  std::cout << "\t" << std::setw(25) << "TOTAL" << std::setw(12) << mpi_count
            << std::setw(12) << nlc::rm_unit(mpi_duration) << std::setw(12)
            << nlc::rm_unit(mpi_duration) / nlc::rm_unit(total_duration) * 100;
}

void outputMessages(eta::Trace const &t,
                    std::vector<struct message> const &msg_vector) {
  /* comm_matrix
   *   [src][dst].first:  message count sent from src to dst
   *   [src][dst].second: total size of messages sent from src to dst
   */
  std::map<eta::ContainerId, std::map<eta::ContainerId, std::pair<int, int>>>
      comm_matrix;
  std::vector<eta::ContainerId> ct_ids;

  for (auto const &c : t.containers) {
    if (c.is_terminal) {
      if (std::find(ct_ids.begin(), ct_ids.end(), c.id) == ct_ids.end()) {
        ct_ids.emplace_back(c.id);
      }
    }
  }

  for (auto const &src : ct_ids) {
    for (auto const &dst : ct_ids) {
      comm_matrix[src].emplace(dst, std::make_pair(0, 0));
    }
  }

  // dump message list in a file
  std::ofstream msg_file("data_messages.dat");

  msg_file << "src\tdst\tlen\ttag\ttype\tstart time\tend time\tthroughput\n";

  for (auto const &m : msg_vector) {
    comm_matrix[m.src][m.dst].first++;
    comm_matrix[m.src][m.dst].second += m.len;

    auto msg_duration = m.endDate - m.startDate;
    double throughput = 0;

    if (msg_duration > 0) {
      throughput = m.len / nlc::rm_unit(m.endDate - m.startDate);
    }

    msg_file << m.src << "\t" << m.dst << "\t" << m.len << "\t" << m.tag << "\t"
             << m.type << "\t" << nlc::rm_unit(m.startDate) << "\t\t"
             << nlc::rm_unit(m.endDate) << "\t\t" << throughput << "\n";
  }

  msg_file.close();

  std::vector<eta::ContainerId> to_delete;

  for (auto const &p1 : comm_matrix) {
    int count = 0;
    if (p1.first.isValid()) {
      for (auto const &p2 : comm_matrix[p1.first]) {
        count += comm_matrix[p1.first][p2.first].first;
        count += comm_matrix[p2.first][p1.first].first;
      }

      if (count == 0) {
        to_delete.emplace_back(p1.first);
      }
    }
  }

  for (auto const &d : to_delete) {
    comm_matrix[d].erase(d);
    comm_matrix.erase(d);
  }

  // write data to plot matrix in a file
  std::ofstream matrix_file_c("data_matrix_count.dat");

  for (auto const &p1 : comm_matrix) {
    for (auto const &p2 : comm_matrix[p1.first]) {
      matrix_file_c << p2.second.first << " ";
    }
    matrix_file_c << "\n";
  }

  matrix_file_c.close();

  std::ofstream matrix_file_s("data_matrix_size.dat");

  for (auto const &p1 : comm_matrix) {
    for (auto const &p2 : comm_matrix[p1.first]) {
      matrix_file_s << p2.second.second << " ";
    }
    matrix_file_s << "\n";
  }

  matrix_file_s.close();

  // display messages to have more information about what happened
  std::cout
      << "Messages dumped in data_matrix_count.dat and data_matrix_size.dat\n";
  std::cout << "Use gnuplot plot.plt to display MPI communication matrix\n";

  int i = 0;
  for (auto const &p : comm_matrix) {
    std::cout << i << ": " << p.first << " -> " << t.container(p.first).name
              << "\n";
    i++;
  }
}

void outputStats(msgstat stat) {
  std::cout << "min: " << stat.min << " ; max: " << stat.max;

  if (stat.count > 0)
    std::cout << " ; avg: " << stat.total / stat.count;

  std::cout << " ; total: " << stat.total;
}

void updateMsgStats(msgstat &to, msgstat const &from) {
  if (from.count != 0) {
    if (to.count == 0) {
      to.min = from.min;
    }

    to.count += from.count;
    to.total += from.total;
    to.min = std::min(from.min, to.min);
    to.max = std::max(from.max, to.max);
  }
}

void updateTimeStats(timestat &to, timestat const &from) {
  if (to.count == 0) {
    to.min = from.min;
  }

  to.count += from.count;
  to.total += from.total;

  to.min = std::min(to.min, from.min);
  to.max = std::max(to.max, from.max);
}

void outputDetails(
    eta::Date const &total_duration, msgstat const &msg_stats,
    std::unordered_map<std::string, timestat> const &fstats_map) {
  std::cout << "\tTotal duration: " << nlc::rm_unit(total_duration) << "\n";
  std::cout << "\tMessages sent: " << msg_stats.count << "\n";
  std::cout << "\tMessages size (in bytes): ";
  outputStats(msg_stats);
  std::cout << "\n";
  outputFunctions(fstats_map, total_duration);
  std::cout << "\n";
}

void outputThreadResults(
    std::unordered_map<std::string, ct_stats> const &stats) {
  for (auto const &p : stats) {
    if (!p.second.fstats_map.empty()) {
      auto name = "Process #" + std::to_string(p.second.process_id) +
                  ": thread #" + std::to_string(p.second.thread_id);
      name.erase(remove(name.begin(), name.end(), '\"'), name.end());

      std::cout << name << "\n";
      outputDetails(p.second.duration, p.second.msg_stats, p.second.fstats_map);
    }
  }
}

void outputProcessResults(
    std::unordered_map<std::string, ct_stats> const &stats) {
  std::unordered_map<int, ct_stats> proc_stats;

  for (auto const &p : stats) {
    // p: ct_name, ct_stats
    proc_stats[p.second.process_id].duration += p.second.duration;

    updateMsgStats(proc_stats[p.second.process_id].msg_stats,
                   p.second.msg_stats);

    for (auto const &f : p.second.fstats_map) {
      // f: function name, time stats
      auto &process_stats = proc_stats[p.second.process_id];
      auto pair = std::find_if(
          process_stats.fstats_map.begin(), process_stats.fstats_map.end(),
          [f](auto const &q) { return f.first == q.first; });

      if (pair == process_stats.fstats_map.end()) {
        process_stats.fstats_map.emplace(f);
      } else {
        updateTimeStats(proc_stats[p.second.process_id].fstats_map[f.first],
                        f.second);
      }
    }
  }

  for (auto const &p : proc_stats) {
    if (!p.second.fstats_map.empty()) {
      std::cout << "Process #" << std::to_string(p.first) << "\n";
      outputDetails(p.second.duration, p.second.msg_stats, p.second.fstats_map);
    }
  }
}

void outputCumulativeResults(
    std::unordered_map<std::string, ct_stats> const &stats) {
  ct_stats global_stats;

  for (auto const &p : stats) {
    // p: ct_name, ct_stats
    updateMsgStats(global_stats.msg_stats, p.second.msg_stats);
    global_stats.duration += p.second.duration;

    for (auto const &f : p.second.fstats_map) {
      // f: function name, stats
      auto pair = std::find_if(
          global_stats.fstats_map.begin(), global_stats.fstats_map.end(),
          [f](auto const &q) { return f.first == q.first; });

      if (pair == global_stats.fstats_map.end()) {
        global_stats.fstats_map.emplace(f);
      } else {
        updateTimeStats(global_stats.fstats_map[f.first], f.second);
      }
    }
  }

  std::cout << "Global stats\n";
  outputDetails(global_stats.duration, global_stats.msg_stats,
                global_stats.fstats_map);
}

void outputResultGlobal(eta::Trace const &t,
                        std::unordered_map<std::string, ct_stats> const &stats,
                        std::vector<struct message> const &msg_vector,
                        bool const &dump_messages, bool const &output_process,
                        bool const &output_cumulative) {
  if (output_cumulative) {
    outputCumulativeResults(stats);
  } else if (output_process) {
    outputProcessResults(stats);
  } else {
    outputThreadResults(stats);
  }

  if (dump_messages) {
    outputMessages(t, msg_vector);
  }
}
