#ifndef OUTPUT_H
#define OUTPUT_H

#include "eta_mpi_stats.h"

void outputResultGlobal(eta::Trace const &t,
                        std::unordered_map<std::string, ct_stats> const &stats,
                        std::vector<struct message> const &msg_vector,
                        bool const &dump_messages, bool const &output_process,
                        bool const &output_cumulative);

#endif // OUTPUT_H
